package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_tambah_dvd.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class TambahDVDActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var uriP : Uri
    lateinit var uriT : Uri
    var fileNameP =""
    var fileTypeP =""
    var fileNameT =""
    var fileTypeT =""
    var suksesposter = 0
    var suksestrailer = 0
    val RC_OK = 100
    var uptype = ""
    var linkP=""
    var linkT=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_dvd)
        btnTambah.setOnClickListener(this)
        imPosterDvdT.setOnClickListener(this)
        imTrailerDvdT.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("dvd")
//        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
//            if (firebaseFireStoreException!=null){
//                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
//            }
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                if(uptype=="jpg"){
                    uriP = data.data!!
                }else if(uptype=="mp4"){
                    uriT = data.data!!
                }
            }
        }
    }

    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnTambah ->{
                val progressDialog = ProgressDialog(this)
                progressDialog.isIndeterminate = true
                progressDialog.setMessage("Mengupload Foto...")
                progressDialog.show()
                if (uriP !=null){
                    fileNameP = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(fileNameP+fileTypeP)
                    fileRef.putFile(uriP)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            linkP=taks.result.toString()
                            suksesposter=1
                            progressDialog.hide()
                            progressDialog.setMessage("Mengupload Video...")
                            progressDialog.show()
                            if (uriT !=null){
                                fileNameT = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                                val fileRef = storage.child(fileNameT+fileTypeT)
                                fileRef.putFile(uriT)
                                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                        return@Continuation fileRef.downloadUrl
                                    })
                                    .addOnCompleteListener { taks ->
                                        linkT=taks.result.toString()
                                        suksestrailer=1
//                                        Log.e("info","isi suksesP+$suksesposter+ suksesT+$suksestrailer")
                                        progressDialog.hide()
                                        progressDialog.setMessage("Menambahkan data...")
                                        progressDialog.show()
                                        if(suksesposter==1 && suksestrailer==1){
                                            val  hm = HashMap<String,Any>()
                                            hm.put("kode", edKdDvdT.text.toString())
                                            hm.put("judul", edJdlFilmT.text.toString())
                                            hm.put("harga",edHrgSewaT.text.toString())
                                            hm.put("tahun",edTahunT.text.toString())
                                            hm.put("poster_name",fileNameP+fileTypeP)
                                            hm.put("poster_url",linkP)
                                            hm.put("trailer_name",fileNameT+fileTypeT)
                                            hm.put("trailer_url",linkT)
                                            db.document(edKdDvdT.text.toString()).set(hm).addOnSuccessListener {
                                                progressDialog.hide()
                                                Toast.makeText(
                                                    this,
                                                    "Data Sukses Ditambahkan",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                                finish()
                                            }
                                        }else{
                                            Toast.makeText(this,"Gagal Menambahkan Data",Toast.LENGTH_SHORT).show()
                                        }
                                    }
                            }
                        }
                }

            }
            R.id.imPosterDvdT ->{
                fileTypeP = ".jpg"
                intent.setType("image/*")
                uptype="jpg"
                startActivityForResult(intent,RC_OK)
            }
            R.id.imTrailerDvdT ->{
                fileTypeT = ".mp4"
                intent.setType("video/*")
                uptype="mp4"
                startActivityForResult(intent,RC_OK)
            }
        }
    }

}