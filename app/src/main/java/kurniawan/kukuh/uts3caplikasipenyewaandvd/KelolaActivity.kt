package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_kelola.*

class KelolaActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: BarangAdapter
    lateinit var uri : Uri
    val DVD_KODE = "kode"
    val DVD_JUDUL = "judul"
    val DVD_HARGA = "harga"
    val DVD_TAHUN = "tahun"
    //    val DVD_POSTER_EXT = "poster_type"
    val DVD_POSTER_URL = "poster_url"
    val RC_OK = 100
    var kode = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kelola)

        btnTambahDVD.setOnClickListener(this)
        lsV.setOnItemClickListener(itemClick)
        alFile = ArrayList()
        uri = Uri.EMPTY
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alFile.get(position)
        kode = hm.get(DVD_KODE).toString()
        Log.i("kode","kode $kode")
        val intent = Intent(this,DetailAdminActivity::class.java)
        intent.putExtra(DVD_KODE,kode)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("dvd")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException!=null){
                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
            }
            showData()
        }
    }

    fun showData(){
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(DVD_KODE, doc.get(DVD_KODE).toString())
                hm.put(DVD_JUDUL, doc.get(DVD_JUDUL).toString())
                hm.put(DVD_HARGA, doc.get(DVD_HARGA).toString())
                hm.put(DVD_TAHUN, doc.get(DVD_TAHUN).toString())
                hm.put(DVD_POSTER_URL, doc.get(DVD_POSTER_URL).toString())
                alFile.add(hm)
            }
            adapter = BarangAdapter(this,alFile)
            lsV.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
            }
        }
    }

    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnTambahDVD ->{
                //panggil tambah dvd
                val intent = Intent(this,TambahDVDActivity::class.java)
                startActivity(intent)
            }
        }
    }
}