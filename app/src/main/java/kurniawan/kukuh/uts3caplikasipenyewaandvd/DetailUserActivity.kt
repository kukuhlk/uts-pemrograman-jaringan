package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_user.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class DetailUserActivity:AppCompatActivity(),View.OnClickListener {

    lateinit var mediaController: MediaController
    lateinit var db : CollectionReference
    lateinit var dba : CollectionReference
    var kode = ""
    var poster = ""
    var trailer = ""
    var username = ""
    var poster_url = ""
    var trailer_url = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahCart ->{
                val  hm = HashMap<String,Any>()
                hm.put("kode", kode)
                hm.put("judul", txJdl.text.toString())
                hm.put("harga",txHrg.text.toString())
                hm.put("tahun",txTahun.text.toString())
                hm.put("poster_name",poster)
                hm.put("poster_url",poster_url)
                hm.put("trailer_name",trailer)
                hm.put("trailer_url",trailer_url)
                hm.put("penyewa",username)
                var fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                dba.document(fileName).set(hm).addOnSuccessListener {
                    Toast.makeText(
                        this,
                        "Sewa DVD Ditambahkan",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_user)
        mediaController = MediaController(this)
        mediaController.setAnchorView(vwTrailer)
        vwTrailer.setMediaController(mediaController)
        var paket : Bundle? = intent.extras
        kode = paket?.getString("kode").toString()
        username = paket?.getString("username").toString()
        btnTambahCart.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance().collection("dvd")
        dba = FirebaseFirestore.getInstance().collection("detail_trx")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore",e.message)
            showDetailDVD(kode)
        }
    }

    fun showDetailDVD(kd:String){
        db.whereEqualTo("kode",kd).get().addOnSuccessListener { result ->
            for(doc in result){
                txKode.text = kode
                txJdl.text = doc.get("judul").toString()
                txHrg.text = doc.get("harga").toString()
                txTahun.text = doc.get("tahun").toString()
                poster = doc.get("poster_name").toString()
                var uriP = doc.get("poster_url").toString()
                poster_url = uriP
                Picasso.get().load(uriP).into(imPoster)
                trailer = doc.get("trailer_name").toString()
                var uriT = doc.get("trailer_url").toString()
                trailer_url = uriT
                vwTrailer.setVideoURI(Uri.parse(uriT))
//                vwTrailer.seekTo(100)
            }
        }
    }
}