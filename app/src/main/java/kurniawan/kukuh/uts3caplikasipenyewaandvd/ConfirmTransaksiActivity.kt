package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_confirm_sewa.*

class ConfirmTransaksiActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var dbt : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: BarangAdapter
    lateinit var dialog: AlertDialog.Builder
    val DVD_KODE = "kode"
    val DVD_JUDUL = "judul"
    val DVD_HARGA = "harga"
    val DVD_TAHUN = "tahun"
    //    val DVD_POSTER_EXT = "poster_type"
    val DVD_POSTER_URL = "poster_url"
    var kd_trx = ""
    var status = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnConfirmTrx ->{
                val hm = HashMap<String, Any>()
                hm.put("status", "dipinjam")
                dbt.document(kd_trx).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Peminjaman dikonfirmasi",Toast.LENGTH_SHORT).show()
                        finish()
                    }
            }
            R.id.btnBatalTrx ->{
                dialog.setTitle("Batalkan?").setMessage("Hapus data transaksi?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_sewa)
        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        kd_trx = paket?.getString("kode_trx").toString()
        btnConfirmTrx.setOnClickListener(this)
        btnBatalTrx.setOnClickListener(this)
        alFile = ArrayList()
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("detail_trx")
        dbt = FirebaseFirestore.getInstance().collection("trx")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message)
            showPinjam(kd_trx)
        }
        dbt.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message)
            showTrx(kd_trx)
        }
    }

    fun showPinjam(kd:String){
        db.whereEqualTo("kode_trx",kd).get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(DVD_KODE, doc.get(DVD_KODE).toString())
                hm.put(DVD_JUDUL, doc.get(DVD_JUDUL).toString())
                hm.put(DVD_HARGA, doc.get(DVD_HARGA).toString())
                hm.put(DVD_TAHUN, doc.get(DVD_TAHUN).toString())
                hm.put(DVD_POSTER_URL, doc.get(DVD_POSTER_URL).toString())
                alFile.add(hm)
            }
            adapter = BarangAdapter(this,alFile)
            lsSewa.adapter = adapter
        }
    }

    fun showTrx(kd:String){
        dbt.whereEqualTo("kode_trx",kd).get().addOnSuccessListener { result ->
            for (doc in result){
                txKdTrans.setText(kd_trx)
                txNamaCust.setText(doc.get("penyewa").toString())
                status = doc.get("status").toString()
                txStatusTrans.setText(status)
                txTotal2.setText(doc.get("total").toString())
            }
            if(status=="dipinjam"){
                btnConfirmTrx.visibility = View.GONE
                btnBatalTrx.visibility = View.GONE
            }
        }
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
        db.whereEqualTo("kode_trx",kd_trx).get()
            .addOnSuccessListener {results ->
                for (doc in results){
                    db.document(doc.id).delete()
                        .addOnSuccessListener {

                        }
                        .addOnFailureListener { e ->
                            Toast.makeText(this,"Data Gagal Dihapus : ${e.message}",Toast.LENGTH_SHORT)
                                .show() }
                }
            }
        dbt.document(kd_trx).delete()
            .addOnSuccessListener {
                Toast.makeText(this,"Data Sukses Dihapus",Toast.LENGTH_SHORT)
                    .show()
                finish()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this,"Data Gagal Dihapus : ${e.message}",Toast.LENGTH_SHORT)
                    .show() }

    }
}