package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity: AppCompatActivity(),View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        var paket : Bundle? = intent.extras
        var teks = paket?.getString("username")
        Log.i("info","$teks")

        btnCariTrx.setOnClickListener(this)
        btnKelola.setOnClickListener(this)
        btnLogoff.setOnClickListener(this)
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("trx")
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnKelola ->{
                val intent = Intent(this,KelolaActivity::class.java)
                startActivity(intent)
            }
            R.id.btnCariTrx ->{
                var kd_trx = edKdTrx.text.toString()
                db.whereEqualTo("kode_trx",kd_trx).get().addOnSuccessListener { result ->
                    var cek = ""
                    for(doc in result){
                        cek = doc.get("kode_trx").toString()
                    }
                    if (cek=="")
                    {
                        Toast.makeText(this,"Kode Transaksi tidak ditemukan !",Toast.LENGTH_SHORT).show()
                    }else{
                        val intent = Intent(this,ConfirmTransaksiActivity::class.java)
                        intent.putExtra("kode_trx",kd_trx)
                        startActivity(intent)
                    }
                }
            }
            R.id.btnLogoff ->{
                fbAuth.signOut()
                //menutup halaman ketika selesai logoff
                Toast.makeText(this,"Successfully Logoff", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}