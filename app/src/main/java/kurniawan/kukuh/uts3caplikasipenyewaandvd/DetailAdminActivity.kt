package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_admin.*

class DetailAdminActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mediaController: MediaController
    lateinit var dialog: AlertDialog.Builder
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    val DVD_KODE = "kode"
    var kode = ""
    var poster = ""
    var trailer = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_admin)
        mediaController = MediaController(this)
        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        kode = paket?.getString(DVD_KODE).toString()
        btnEditDVD.setOnClickListener(this)
        btnHapusDVD.setOnClickListener(this)
//        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(vwTrailer)
        vwTrailer.setMediaController(mediaController)
//        videoSet(posVidSkrg)
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("dvd")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore",e.message)
            showDetailDVD(kode)
        }
    }

    fun showDetailDVD(kd:String){
        db.whereEqualTo("kode",kd).get().addOnSuccessListener { result ->
            for(doc in result){
                txKode.text = kode
                txJdl.text = doc.get("judul").toString()
                txHrg.text = doc.get("harga").toString()
                txTahun.text = doc.get("tahun").toString()
                poster = doc.get("poster_name").toString()
                var uriP = doc.get("poster_url").toString()
                Picasso.get().load(uriP).into(imPoster)
                trailer = doc.get("trailer_name").toString()
                var uriT = doc.get("trailer_url").toString()
                vwTrailer.setVideoURI(Uri.parse(uriT))
//                vwTrailer.seekTo(100)
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnHapusDVD ->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnEditDVD ->{
                val intent = Intent(this,EditDVDActivity::class.java)
                intent.putExtra(DVD_KODE,kode)
                startActivity(intent)
            }
        }
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->

        val fileRefP = storage.child(poster)
        fileRefP.delete().addOnSuccessListener {

        }.addOnFailureListener {

        }
        val fileRefT = storage.child(trailer)
        fileRefT.delete().addOnSuccessListener {

        }.addOnFailureListener {

        }
        db.whereEqualTo("kode",kode).get()
            .addOnSuccessListener {results ->
                for (doc in results){
                    db.document(doc.id).delete()
                        .addOnSuccessListener {
                            Toast.makeText(this,"Data Sukses Dihapus",Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        }
                        .addOnFailureListener { e ->
                            Toast.makeText(this,"Data Gagal Dihapus : ${e.message}",Toast.LENGTH_SHORT)
                                .show() }
                }
            }.addOnFailureListener { e ->
                Toast.makeText(this,"Tidak bisa menemukan referensi : ${e.message}",Toast.LENGTH_SHORT)
                    .show()
            }
    }
}