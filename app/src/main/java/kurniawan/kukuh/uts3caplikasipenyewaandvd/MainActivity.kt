package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener{

    val RC_REGISTER_SUKSES : Int = 100
    var email = ""
    var password = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin->{
                email = edUserName.text.toString()
                password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Username / password can't be empty",Toast.LENGTH_LONG).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            if (email!="admin@admin.com"){
                                val intent = Intent(this,UserActivity::class.java)
                                intent.putExtra("username",edUserName.text.toString())
                                edUserName.setText("")
                                edPassword.setText("")
                                edUserName.requestFocus()
                                Toast.makeText(this,"Successfully Login",Toast.LENGTH_SHORT).show()
                                startActivity(intent)
                            }else{
                                val intent = Intent(this,AdminActivity::class.java)
                                edUserName.setText("")
                                edPassword.setText("")
                                edUserName.requestFocus()
                                Toast.makeText(this,"Successfully Login",Toast.LENGTH_SHORT).show()
                                startActivity(intent)
                            }

                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this,"Incorrect username / password",Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.txSignUp->{
                val intent = Intent(this,RegisterActivity::class.java)
                startActivityForResult(intent,RC_REGISTER_SUKSES)
            }
        }
    }

    //mengisi username password jika sukses register
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode== Activity.RESULT_OK){
            if(requestCode == RC_REGISTER_SUKSES){
                edUserName.setText(data?.extras?.getString("username"))
                edPassword.setText(data?.extras?.getString("password"))
            }
        }
    }
}