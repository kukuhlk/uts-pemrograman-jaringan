package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_edit_dvd.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class EditDVDActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var uriP : Uri
    lateinit var uriT : Uri
    var fileNameP =""
    var fileTypeP =""
    var fileNameT =""
    var fileTypeT =""
    var suksesposter = 0
    var suksestrailer = 0
    val RC_OK = 100
    var uptype = ""
    var linkP=""
    var linkT=""
    var kode=""
    val DVD_KODE="kode"
    var poster=""
    var trailer=""
    var inisiasi=1

    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnEdit -> {
                val progressDialog = ProgressDialog(this)
                progressDialog.isIndeterminate = true
                progressDialog.setMessage("Mengupload Foto...")
                progressDialog.show()
                if (uriP !=null) {
                    fileNameP = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(fileNameP + fileTypeP)
                    fileRef.putFile(uriP)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            //delete poster lama
                            val fileRefP = storage.child(poster)
                            fileRefP.delete().addOnSuccessListener {
                            }.addOnFailureListener {
                            }
                            linkP = taks.result.toString()
                            suksesposter = 1
                            progressDialog.hide()
                            progressDialog.setMessage("Mengupload Video...")
                            progressDialog.show()
                            if (uriT != null) {
                                fileNameT = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                                val fileRef = storage.child(fileNameT + fileTypeT)
                                fileRef.putFile(uriT)
                                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                        return@Continuation fileRef.downloadUrl
                                    })
                                    .addOnCompleteListener { taks ->
                                        //delete trailer lama
                                        val fileRefT = storage.child(trailer)
                                        fileRefT.delete().addOnSuccessListener {
                                        }.addOnFailureListener {
                                        }
                                        linkT = taks.result.toString()
                                        suksestrailer = 1
                                        progressDialog.hide()
                                        progressDialog.setMessage("Mengupdate data...")
                                        progressDialog.show()
                                        if (suksesposter == 1 && suksestrailer == 1) {
                                            val hm = HashMap<String, Any>()
                                            hm.put("judul", edJdlFilmE.text.toString())
                                            hm.put("harga", edHrgSewaE.text.toString())
                                            hm.put("tahun", edTahunE.text.toString())
                                            hm.put("poster_name", fileNameP + fileTypeP)
                                            hm.put("poster_url", linkP)
                                            hm.put("trailer_name", fileNameT + fileTypeT)
                                            hm.put("trailer_url", linkT)
                                            db.document(kode).update(hm)
                                                .addOnSuccessListener {
                                                    progressDialog.hide()
                                                    Toast.makeText(
                                                        this,
                                                        "Data Sukses Diupdate",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                    finish()
                                                }
                                        } else {
                                            Toast.makeText(
                                                this,
                                                "Gagal Mengupdate Data",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                    }
                            }
                        }
                }
            }
            R.id.imPosterDvdE ->{
                fileTypeP = ".jpg"
                intent.setType("image/*")
                uptype="jpg"
                startActivityForResult(intent,RC_OK)
            }
            R.id.imTrailerDvdE ->{
                fileTypeT = ".mp4"
                intent.setType("video/*")
                uptype="mp4"
                startActivityForResult(intent,RC_OK)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                if(uptype=="jpg"){
                    uriP = data.data!!
                }else if(uptype=="mp4"){
                    uriT = data.data!!
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_dvd)
        var paket : Bundle? = intent.extras
        kode = paket?.getString(DVD_KODE).toString()
        btnEdit.setOnClickListener(this)
        imPosterDvdE.setOnClickListener(this)
        imTrailerDvdE.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("dvd")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore",e.message)
            if(inisiasi==1){showDetailDVD(kode)}
        }
    }

    fun showDetailDVD(kd:String){
        db.whereEqualTo("kode",kd).get().addOnSuccessListener { result ->
            for(doc in result){
                edKdDvdE.setText(kode)
                edJdlFilmE.setText(doc.get("judul").toString())
                edHrgSewaE.setText(doc.get("harga").toString())
                edTahunE.setText(doc.get("tahun").toString())
                poster = doc.get("poster_name").toString()
                trailer = doc.get("trailer_name").toString()
//                vwTrailer.seekTo(100)
            }
        }
        inisiasi=0
    }
}