package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class BarangAdapter (val context : Context,
                     arrayList: ArrayList<HashMap<String, Any>>): BaseAdapter() {
    val DVD_KODE = "kode"
    val DVD_JUDUL = "judul"
    val DVD_HARGA = "harga"
    val DVD_TAHUN = "tahun"
    val DVD_POSTER_URL = "poster_url"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txKode : TextView? = null
        var txJudul : TextView? = null
        var txHarga : TextView? = null
        var txTahun : TextView? = null
        var imv : ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view:View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_barang, null, true)

            holder.txKode = view!!.findViewById(R.id.txKode) as TextView
            holder.txJudul = view!!.findViewById(R.id.txJudul) as TextView
            holder.txHarga = view!!.findViewById(R.id.txHarga) as TextView
            holder.txTahun = view!!.findViewById(R.id.txTahun) as TextView
            holder.imv = view!!.findViewById(R.id.imv) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }
        uri = Uri.parse(list.get(position).get(DVD_POSTER_URL).toString())

        holder.txKode!!.setText(list.get(position).get(DVD_KODE).toString())
        holder.txJudul!!.setText(list.get(position).get(DVD_JUDUL).toString())
        holder.txHarga!!.setText("Rp " + list.get(position).get(DVD_HARGA).toString())
        holder.txTahun!!.setText(list.get(position).get(DVD_TAHUN).toString())

        Picasso.get().load(uri).into(holder.imv)

        return view!!

    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }

}