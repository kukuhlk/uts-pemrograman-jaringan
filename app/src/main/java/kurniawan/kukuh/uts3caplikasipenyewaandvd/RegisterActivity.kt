package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), View.OnClickListener  {
    override fun onClick(v: View?) {
        var email = edRegUserName.text.toString()
        var password = edRegPassword.text.toString()

        if (email.isEmpty() || password.isEmpty()){
            Toast.makeText(this,"Username / password can't be empty", Toast.LENGTH_LONG).show()
        }else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this,"Successfully Register", Toast.LENGTH_SHORT).show()
                    //kembali ke menu login
                    finish()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this,"Username exist / password less than 6 character", Toast.LENGTH_SHORT).show()
                }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        btnRegister.setOnClickListener(this)
    }

    //mengembalikan nilai username password ke login jika sukses register
    override fun finish() {
        var intent = Intent()
        intent.putExtra("username",edRegUserName.text.toString())
        intent.putExtra("password",edRegPassword.text.toString())
        setResult(Activity.RESULT_OK,intent)

        super.finish()
    }
}