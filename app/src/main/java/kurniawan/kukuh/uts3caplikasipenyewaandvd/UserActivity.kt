package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity: AppCompatActivity(), View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var db : CollectionReference
    lateinit var dbt : CollectionReference
    var username = ""
    var status = ""
    var kd_trx = ""
    var total = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        var paket : Bundle? = intent.extras
        username = paket?.getString("username").toString()
//        Log.i("info","$teks")
        txUsername.setText(username)


        db = FirebaseFirestore.getInstance().collection("dvd")
        dbt = FirebaseFirestore.getInstance().collection("trx")
        btnCart.setOnClickListener(this)
        btnCariDVD.setOnClickListener(this)
        btnLogoff.setOnClickListener (this)
    }

    override fun onStart() {
        super.onStart()
        cekSewa(username)
    }

    fun cekSewa(un: String){
        dbt.whereEqualTo("penyewa",un).get().addOnSuccessListener { result ->
            for(doc in result){
                kd_trx = doc.get("kode_trx").toString()
                status = doc.get("status").toString()
                total = doc.get("total").toString().toInt()
            }
            if(kd_trx!=""){
                textView16.visibility = View.GONE
                textInputLayout5.visibility = View.GONE
                edKdDVD.visibility = View.GONE
                btnCariDVD.visibility = View.GONE
                btnCart.visibility = View.GONE
                txKdTrx.text = kd_trx
                Log.d("status","status $status")

                textView10.visibility = View.VISIBLE
                txKdTrx.visibility = View.VISIBLE
                txPesan.visibility = View.VISIBLE
                if(status=="proses"){
                    txPesan.text = "Tunjukkan kode transaksi anda ke kasir beserta dvd yang akan anda sewa."
                }else{
                    txPesan.text = "Penyewaan DVD Berhasil. Jangan lupa untuk mengembalikan tepat waktu."
                }
            }else{
                textView16.visibility = View.VISIBLE
                textInputLayout5.visibility = View.VISIBLE
                edKdDVD.visibility = View.VISIBLE
                btnCariDVD.visibility = View.VISIBLE
                btnCart.visibility = View.VISIBLE

                textView10.visibility = View.GONE
                txKdTrx.visibility = View.GONE
                txPesan.visibility = View.GONE
            }
        }.addOnFailureListener {
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnCariDVD->{
                var kd_dvd = edKdDVD.text.toString()
                val progressDialog = ProgressDialog(this)
                progressDialog.isIndeterminate = true
                progressDialog.setMessage("Mencari...")
                progressDialog.show()
                db.whereEqualTo("kode",kd_dvd).get().addOnSuccessListener { result ->
                    progressDialog.hide()
                    val intent = Intent(this,DetailUserActivity::class.java)
                    intent.putExtra("kode",kd_dvd)
                    intent.putExtra("username",username)
                    startActivity(intent)
                }.addOnFailureListener {
                    Toast.makeText(this,"Kode DVD tidak ditemukan !",Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnCart->{
                val intent = Intent(this,CartActivity::class.java)
                intent.putExtra("username",username)
                startActivity(intent)
            }
            R.id.btnLogoff -> {
                fbAuth.signOut()
                //menutup halaman ketika selesai logoff
                Toast.makeText(this,"Successfully Logoff",Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}