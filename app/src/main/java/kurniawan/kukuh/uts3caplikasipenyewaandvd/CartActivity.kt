package kurniawan.kukuh.uts3caplikasipenyewaandvd

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_cart.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CartActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var dbt : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: BarangAdapter
    val DVD_KODE = "kode"
    val DVD_JUDUL = "judul"
    val DVD_HARGA = "harga"
    val DVD_TAHUN = "tahun"
    //    val DVD_POSTER_EXT = "poster_type"
    val DVD_POSTER_URL = "poster_url"
    var username = ""
    var status = ""
    var total = 0
    var inisiasi=1

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSewa ->{
                val  hm = HashMap<String,Any>()
                var fileName = "TRX_" + SimpleDateFormat("yyMMddHHmmssSSS").format(Date())
                hm.put("kode_trx",fileName)
                hm.put("status","proses")
                hm.put("penyewa",username)
                hm.put("total",total)
                dbt.document(fileName).set(hm).addOnSuccessListener {
                    db.whereEqualTo("penyewa",username).get().addOnSuccessListener { result ->
                        val  hm = HashMap<String,Any>()
                        hm.put("kode_trx",fileName)
                        for(doc in result){
                            db.document(doc.id).update(hm)
                                .addOnSuccessListener {
                                    Log.d("update","update data $doc.id")
                                }
                        }
                    }
                    Toast.makeText(
                        this,
                        "Sewa DVD Berhasil",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        var paket : Bundle? = intent.extras
        username = paket?.getString("username").toString()
        btnSewa.setOnClickListener(this)
        alFile = ArrayList()
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("detail_trx")
        dbt = FirebaseFirestore.getInstance().collection("trx")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message)
            if(inisiasi==1){
                showPinjam(username)}
        }
    }

    fun showPinjam(un:String){
        db.whereEqualTo("penyewa",un).get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(DVD_KODE, doc.get(DVD_KODE).toString())
                hm.put(DVD_JUDUL, doc.get(DVD_JUDUL).toString())
                var harga = doc.get(DVD_HARGA).toString()
                total = total + harga.toInt()
                hm.put(DVD_HARGA, doc.get(DVD_HARGA).toString())
                hm.put(DVD_TAHUN, doc.get(DVD_TAHUN).toString())
                hm.put(DVD_POSTER_URL, doc.get(DVD_POSTER_URL).toString())
                alFile.add(hm)
                Log.d("username","un = $un")
                Log.d("hm","hm = $hm")
            }
            adapter = BarangAdapter(this,alFile)
            lsCart.adapter = adapter
            txTotal.text = "Rp " + total.toString()
            inisiasi=0
        }
    }
}